from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

opts = webdriver.ChromeOptions()
#opts.add_extension(proxy_chrome_extension)
#opts.add_argument("user-agent=Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Mobile Safari/537.36")
opts.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
opts.add_argument("--mute-audio")
opts.add_argument('--dns-prefetch-disable')
opts.add_argument('--lang=en-US')
opts.add_argument('--disable-setuid-sandbox')
#opts.add_argument('--headless')
#opts.add_argument('--proxy-server=%s' % PROXY)
cap = DesiredCapabilities.CHROME
driver = webdriver.Chrome(chrome_options=opts, executable_path='/home/lorenzo/InstaPy/assets/chromedriver', desired_capabilities=cap)

LOGIN = 'lorenzobogoni94@gmail.com'
PASSWORD = ''

driver.get('https://www.linkedin.com/uas/login?session_redirect=%2Fvoyager%2FloginRedirect%2Ehtml&fromSignIn=true&trk=uno-reg-join-sign-in')

email = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="username"]')))
email.send_keys(LOGIN)
password = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="password"]')))
password.send_keys(PASSWORD)
login = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="app__container"]/main/div/form/div[3]/button')))
login.click()

tempWait = WebDriverWait(driver, 1)
tempWait2 = WebDriverWait(driver, 2)
try:
    tempWait2.until(EC.presence_of_element_located((By.CLASS_NAME, 'dicoboh')))
except TimeoutException as e:
    pass

url_links = ''

driver.get(url_links)

try:
    tempWait2.until(EC.presence_of_element_located((By.CLASS_NAME, 'dicoboh')))
except TimeoutException as e:
    pass

driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

n_pages = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="ember150"]/artdeco-pagination/ul/li[10]/button/span'))).text

print(n_pages)

with open('list.csv', 'a') as f:
    for i in range(1, int(n_pages)):
        for person in driver.find_elements_by_class_name('search-result__occluded-item'):
            driver.execute_script("arguments[0].scrollIntoView(true);", person)
            name = person.find_element_by_class_name('actor-name').text
            url = person.find_element_by_class_name('search-result__result-link').get_attribute("href")
            job = person.find_element_by_class_name('subline-level-1').text
            location = person.find_element_by_class_name('subline-level-2').text
            string = name + ';' + location + ';' + job + ';' + url + '\n'
            print(string)
            f.write(string.encode('utf-8'))
            try:
                tempWait.until(EC.presence_of_element_located((By.CLASS_NAME, 'dicoboh')))
            except TimeoutException as e:
                pass
        tempWait3 = WebDriverWait(driver, 3)
        try:
            tempWait3.until(EC.presence_of_element_located((By.CLASS_NAME, 'dicoboh')))
        except TimeoutException as e:
            pass
        if(i+1 <= int(n_pages)):
            driver.get('https://www.linkedin.com/search/results/people/?facetConnectionOf=%5B%22ACoAAAidwtQBZGPMw5R2Ux8Vi5-RkT2dQcNVsa8%22%5D&facetNetwork=%5B%22F%22%2C%22S%22%5D&origin=MEMBER_PROFILE_CANNED_SEARCH&page='+str(i+1))
            try:
                tempWait3.until(EC.presence_of_element_located((By.CLASS_NAME, 'dicoboh')))
            except TimeoutException as e:
                pass
f.close()